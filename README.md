# OCR MODEL

This project uses a convolutional neural network to preform optical character recognition on screenshots of snippets of text that are 10 characters long. The train and test data consist of images of randomly generated strings, with the random string as the label. The labels are encoded as binary arrays. The architecture of this network includes two convolutional layers followed by max pooling followed by two dense layers. The output dimensions are 10 x 62, which is the number of characters in each string by the number of all possible characters. This trained model can be used to predict the labels of new images, and the labels can be decoded into a string of plain text. The parameters in these notebooks can be adjusted to improve the models.

## Notebooks Included

### string_model_final

This notebook generates samples using multiple different fonts. The fonts used can be found in the 'test' folder. Using the architecture described above the model is trained with the encoded labels and images in 30 epochs(iterations). Loss and accuracy scores and graphs are included. This trained model is saved as 'final.h5'. (Previously trained with more samples but version crashed).

### default_font_short_train

This notebook is similar to the first, however only creates samples for one default font. This can be used incase the fonts included are difficult to work with. This trained model is saved as 'default.h5'.  This model is very accurate but usually only predicts for one or two characters in the string as is. (Previously trained models predicted more characters, but were less accurate).

### model_predictor

This notebook can be used to make predictions on new images. It includes a function to decode the encoded labels. The prediction function can be given a saved model and a new image. This function prints the image along with a decoded prediction. This notebook also includes the code to create a random test image.

### label_encoder_examples

This notebook displays examples of how the labels are encoded and tests to ensure that the labels are encoded correctly. 

## Built With

* [Keras](https://keras.io) - neural network API

## Acknowledgments

* based on daveshap ocr_svc github repository; single character OCR
